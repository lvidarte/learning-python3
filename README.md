# Learning Python3

Try it out on Binder! [![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/lvidarte%2Flearning-python3/master?filepath=Index.ipynb)

You can also get this tutorial and run it on your laptop:

## Clone the repo

    git clone https://gitlab.com/lvidarte/learning-python3.git

## Create environment

    cd learning-python3
    python3 -m venv env

## Activate environment

    source env/bin/activate

## Install packages

    pip install --upgrade pip
    pip install -r requirements.txt

## Run notebook

    jupyter notebook

